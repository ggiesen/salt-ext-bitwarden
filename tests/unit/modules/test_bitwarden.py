# pylint: disable=too-many-lines
from unittest.mock import MagicMock
from unittest.mock import patch

import pytest
import saltext.bitwarden.modules.bitwarden_mod as bitwarden_module


@pytest.fixture
def configure_loader_modules():
    module_globals = {
        "__salt__": {
            "config.get": MagicMock(
                return_value={
                    "driver": "bitwarden",
                    "cli_path": "/bin/bw",
                    "cli_conf_dir": "/etc/salt/.bitwarden",
                    "cli_runas": "salt",
                    "vault_url": "https://bitwarden.com",
                    "email": "user@example.com",
                    "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
                    "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
                },
            ),
        }
    }
    return {
        bitwarden_module: module_globals,
    }


@pytest.fixture
def profile():
    return {
        "driver": "bitwarden",
        "cli_path": "/bin/bw",
        "cli_conf_dir": "/etc/salt/.bitwarden",
        "cli_runas": "salt",
        "vault_url": "https://bitwarden.com",
        "email": "user@example.com",
        "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
        "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
    }


@pytest.fixture
def get_item_results():
    return {
        "object": "item",
        "id": "e276050e-1c71-42ac-95eb-ad3300f63840",
        "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
        "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
        "type": 1,
        "reprompt": 0,
        "name": "Test Object",
        "notes": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
        "favorite": False,
        "fields": [
            {"name": "test_text", "value": "test", "type": 0, "linked_id": None},
            {"name": "test_hidden", "value": "test", "type": 1, "linked_id": None},
            {"name": "test_boolean", "value": "true", "type": 2, "linked_id": None},
            {"name": "test_linked_username", "value": None, "type": 3, "linked_id": 100},
            {"name": "test_linked_password", "value": None, "type": 3, "linked_id": 101},
        ],
        "login": {
            "uris": [
                {"match": 1, "uri": "https://test.example.com"},
                {"match": 0, "uri": "https://test2.example.com"},
                {"match": 2, "uri": "https://test3.example.com"},
                {"match": 4, "uri": "https://test4.example.com"},
                {"match": 3, "uri": "https://test5.example.com"},
                {"match": 5, "uri": "https://test6.example.com"},
                {"match": None, "uri": "https://test7.example.com"},
            ],
            "username": "test",
            "password": "CorrectHorseBatteryStaple",
            "totp": "JBSWY3DPEHPK3PXP",
            "password_revision_date": "2023-01-30T05:32:50.470Z",
        },
        "collection_ids": [],
        "attachments": [
            {
                "id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                "file_name": "test.txt",
                "size": "81",
                "size_name": "81 Bytes",
                "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
            }
        ],
        "creation_date": "2023-01-14T05:05:11.666Z",
        "revision_date": "2023-01-30T05:32:51.443Z",
        "deleted_date": None,
        "password_history": [
            {"last_used_date": "2023-01-30T05:32:50.470Z", "password": "testtesttest"},
            {"last_used_date": "2023-01-14T06:46:37.112Z", "password": "testtest"},
        ],
    }


@pytest.fixture
def list_items_results():
    return [
        {
            "object": "item",
            "id": "b8bb12f4-b378-4e56-b86d-af48005e13c2",
            "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
            "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
            "type": 1,
            "reprompt": 0,
            "name": "Example Login Item",
            "notes": "Example notes for login item",
            "favorite": False,
            "login": {
                "uris": [],
                "username": "jdoe",
                "password": "CorrectHorseBatteryStaple",
                "totp": None,
                "password_revision_date": None,
            },
            "collection_ids": [],
            "revision_date": "2022-11-09T05:42:31.470Z",
            "deleted_date": None,
        },
        {
            "object": "item",
            "id": "e276050e-1c71-42ac-95eb-ad3300f63840",
            "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
            "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
            "type": 1,
            "reprompt": 0,
            "name": "Test Object",
            "notes": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
            "favorite": False,
            "fields": [
                {"name": "test_text", "value": "test", "type": 0, "linked_id": None},
                {"name": "test_hidden", "value": "test", "type": 1, "linked_id": None},
                {"name": "test_boolean", "value": "true", "type": 2, "linked_id": None},
                {"name": "test_linked_username", "value": None, "type": 3, "linked_id": 100},
                {"name": "test_linked_password", "value": None, "type": 3, "linked_id": 101},
            ],
            "login": {
                "uris": [
                    {"match": 1, "uri": "https://test.example.com"},
                    {"match": 0, "uri": "https://test2.example.com"},
                    {"match": 2, "uri": "https://test3.example.com"},
                    {"match": 4, "uri": "https://test4.example.com"},
                    {"match": 3, "uri": "https://test5.example.com"},
                    {"match": 5, "uri": "https://test6.example.com"},
                    {"match": None, "uri": "https://test7.example.com"},
                ],
                "username": "test",
                "password": "testtesttest",
                "totp": "JBSWY3DPEHPK3PXP",
                "password_revision_date": "2023-01-14T06:46:37.112Z",
            },
            "collection_ids": [],
            "attachments": [
                {
                    "id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                    "file_name": "test.txt",
                    "size": "81",
                    "size_name": "81 Bytes",
                    "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                }
            ],
            "revision_date": "2023-01-14T07:05:22.130Z",
            "deleted_date": None,
            "password_history": [
                {"last_used_date": "2023-01-14T06:46:37.112Z", "password": "testtest"}
            ],
        },
    ]


def test_login_successful_then_return_true(profile):

    expected_result = True

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.login", autospec=True
    ) as login:
        login.return_value = True

        actual_result = bitwarden_module.login(profile=profile)

        assert actual_result == expected_result


def test_login_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.login", autospec=True
    ) as login:
        login.return_value = False

        actual_result = bitwarden_module.login(profile=profile)

        assert actual_result == expected_result


def test_logout_successful_then_return_true(profile):

    expected_result = True

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.logout", autospec=True
    ) as logout:
        logout.return_value = True

        actual_result = bitwarden_module.logout(profile=profile)

        assert actual_result == expected_result


def test_logout_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.logout", autospec=True
    ) as logout:
        logout.return_value = False

        actual_result = bitwarden_module.logout(profile=profile)

        assert actual_result == expected_result


def test_lock_successful_then_return_true(profile):

    expected_result = True

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.lock", autospec=True
    ) as lock:
        lock.return_value = True

        actual_result = bitwarden_module.lock(profile=profile)

        assert actual_result == expected_result


def test_lock_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.lock", autospec=True
    ) as lock:
        lock.return_value = False

        actual_result = bitwarden_module.lock(profile=profile)

        assert actual_result == expected_result


def test_unlock_successful_then_return_true(profile):

    expected_result = True

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.unlock", autospec=True
    ) as unlock:
        unlock.return_value = True

        actual_result = bitwarden_module.unlock(profile=profile)

        assert actual_result == expected_result


def test_unlock_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.unlock", autospec=True
    ) as unlock:
        unlock.return_value = False

        actual_result = bitwarden_module.unlock(profile=profile)

        assert actual_result == expected_result


def test_get_item_successful_then_return_dict(profile, get_item_results):

    expected_result = get_item_results

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_module.get_item(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_item_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = False

        actual_result = bitwarden_module.get_item(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_list_items_successful_then_return_list(profile, list_items_results):

    expected_result = list_items_results

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_items", autospec=True
    ) as list_items:
        list_items.return_value = list_items_results

        actual_result = bitwarden_module.list_items(profile=profile)

        assert actual_result == expected_result


def test_list_items_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_items", autospec=True
    ) as list_items:
        list_items.return_value = False

        actual_result = bitwarden_module.list_items(profile=profile)

        assert actual_result == expected_result


def test_get_username_successful_then_return_string(profile):

    expected_result = "test"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "test"

        actual_result = bitwarden_module.get_username(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_username_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_username(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_password_successful_then_return_string(profile):

    expected_result = "CorrectHorseBatteryStaple"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "CorrectHorseBatteryStaple"

        actual_result = bitwarden_module.get_password(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_password_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_password(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_uri_successful_then_return_string(profile):

    expected_result = "https://test.example.com"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "https://test.example.com"

        actual_result = bitwarden_module.get_uri(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_uri_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_uri(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_totp_successful_then_return_string(profile):

    expected_result = "123456"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "123456"

        actual_result = bitwarden_module.get_totp(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_totp_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_totp(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_notes_successful_then_return_string(profile):

    expected_result = (
        "This is a test, multiline note field.\nHere is the second line.\nHere is the third line."
    )

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "This is a test, multiline note field.\nHere is the second line.\nHere is the third line."

        actual_result = bitwarden_module.get_notes(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_notes_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_notes(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_exposed_password_successful_then_return_string(profile):

    expected_result = "1868"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = "1868"

        actual_result = bitwarden_module.get_exposed_password(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_exposed_password_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_item_field",
        autospec=True,
    ) as get_item_field:
        get_item_field.return_value = False

        actual_result = bitwarden_module.get_exposed_password(
            profile=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_folder_successful_then_return_dict(profile):

    expected_result = {
        "object": "folder",
        "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
        "name": "Test",
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_folder", autospec=True
    ) as get_folder:
        get_folder.return_value = {
            "object": "folder",
            "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
            "name": "Test",
        }

        actual_result = bitwarden_module.get_folder(
            profile=profile, folder_id="0d62e546-ec44-4709-b6c6-ad3300f10e11"
        )

        assert actual_result == expected_result


def test_get_folder_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_folder", autospec=True
    ) as get_folder:
        get_folder.return_value = False

        actual_result = bitwarden_module.get_folder(
            profile=profile, folder_id="0d62e546-ec44-4709-b6c6-ad3300f10e11"
        )

        assert actual_result == expected_result


def test_list_folders_successful_then_return_list(profile):

    expected_result = [
        {"object": "folder", "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11", "name": "Test"},
        {
            "object": "folder",
            "id": "b5e12f87-b395-4a26-be2e-aa8d0706eb5a",
            "name": "Another Test Folder",
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_folders", autospec=True
    ) as list_folders:
        list_folders.return_value = [
            {"object": "folder", "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11", "name": "Test"},
            {
                "object": "folder",
                "id": "b5e12f87-b395-4a26-be2e-aa8d0706eb5a",
                "name": "Another Test Folder",
            },
        ]

        actual_result = bitwarden_module.list_folders(profile=profile)

        assert actual_result == expected_result


def test_list_folders_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_folders", autospec=True
    ) as list_folders:
        list_folders.return_value = False

        actual_result = bitwarden_module.list_folders(profile=profile)

        assert actual_result == expected_result


def test_get_send_successful_then_return_dict(profile):

    expected_result = {
        "object": "send",
        "id": "8be1ff82-dd40-4af1-96ae-565467293510",
        "access_id": "cMJbuCb6hUOJN69GGGa9XA",
        "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
        "name": "My Text Send",
        "notes": "Notes for the text send.",
        "key": "rBvU5J6P2GUuaANXyacQ6w==",
        "type": 0,
        "max_access_count": 3,
        "access_count": 0,
        "revision_date": "2022-11-07T05:15:48.503Z",
        "deletion_date": "2022-11-07T06:15:47.930Z",
        "expiration_date": "2022-11-07T06:15:47.933Z",
        "password_set": True,
        "disabled": False,
        "hide_email": True,
        "text": {"text": "Secret Information", "hidden": True},
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_send", autospec=True
    ) as get_send:
        get_send.return_value = {
            "object": "send",
            "id": "8be1ff82-dd40-4af1-96ae-565467293510",
            "access_id": "cMJbuCb6hUOJN69GGGa9XA",
            "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
            "name": "My Text Send",
            "notes": "Notes for the text send.",
            "key": "rBvU5J6P2GUuaANXyacQ6w==",
            "type": 0,
            "max_access_count": 3,
            "access_count": 0,
            "revision_date": "2022-11-07T05:15:48.503Z",
            "deletion_date": "2022-11-07T06:15:47.930Z",
            "expiration_date": "2022-11-07T06:15:47.933Z",
            "password_set": True,
            "disabled": False,
            "hide_email": True,
            "text": {"text": "Secret Information", "hidden": True},
        }

        actual_result = bitwarden_module.get_send(
            profile=profile, send_id="8be1ff82-dd40-4af1-96ae-565467293510"
        )

        assert actual_result == expected_result


def test_get_send_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_send", autospec=True
    ) as get_send:
        get_send.return_value = False

        actual_result = bitwarden_module.get_send(
            profile=profile, send_id="8be1ff82-dd40-4af1-96ae-565467293510"
        )

        assert actual_result == expected_result


def test_list_sends_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "send",
            "id": "8be1ff82-dd40-4af1-96ae-565467293510",
            "access_id": "cMJbuCb6hUOJN69GGGa9XA",
            "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
            "name": "My Text Send",
            "notes": "Notes for the text send.",
            "key": "rBvU5J6P2GUuaANXyacQ6w==",
            "type": 0,
            "max_access_count": 3,
            "access_count": 0,
            "revision_date": "2022-11-07T05:15:48.503Z",
            "deletion_date": "2022-11-07T06:15:47.930Z",
            "expiration_date": "2022-11-07T06:15:47.933Z",
            "password_set": True,
            "disabled": False,
            "hide_email": True,
            "text": {"text": "Secret Information", "hidden": True},
        },
        {
            "object": "send",
            "id": "eebd1611-dd47-47d9-bee2-0ef3891a95e8",
            "access_id": "RpI1hSjOWAqGga9GAHFGJQ",
            "access_url": "https://vault.example.com/#/send/RpI1hSjOWAqGga9GAHFGJQ/GrOZ69q7XwYdeE65t9-szg",
            "name": "My Text Send",
            "notes": "Notes for the text send.",
            "key": "GrOZ69q7XwYdeE65t9+szg==",
            "type": 0,
            "max_access_count": 3,
            "access_count": 0,
            "revision_date": "2022-11-07T06:52:57.123Z",
            "deletion_date": "2022-11-23T19:06:53.810Z",
            "expiration_date": "2022-11-23T19:06:53.810Z",
            "password_set": True,
            "disabled": False,
            "hide_email": True,
            "text": {"text": "Secret Information", "hidden": True},
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_sends", autospec=True
    ) as list_sends:
        list_sends.return_value = [
            {
                "object": "send",
                "id": "8be1ff82-dd40-4af1-96ae-565467293510",
                "access_id": "cMJbuCb6hUOJN69GGGa9XA",
                "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
                "name": "My Text Send",
                "notes": "Notes for the text send.",
                "key": "rBvU5J6P2GUuaANXyacQ6w==",
                "type": 0,
                "max_access_count": 3,
                "access_count": 0,
                "revision_date": "2022-11-07T05:15:48.503Z",
                "deletion_date": "2022-11-07T06:15:47.930Z",
                "expiration_date": "2022-11-07T06:15:47.933Z",
                "password_set": True,
                "disabled": False,
                "hide_email": True,
                "text": {"text": "Secret Information", "hidden": True},
            },
            {
                "object": "send",
                "id": "eebd1611-dd47-47d9-bee2-0ef3891a95e8",
                "access_id": "RpI1hSjOWAqGga9GAHFGJQ",
                "access_url": "https://vault.example.com/#/send/RpI1hSjOWAqGga9GAHFGJQ/GrOZ69q7XwYdeE65t9-szg",
                "name": "My Text Send",
                "notes": "Notes for the text send.",
                "key": "GrOZ69q7XwYdeE65t9+szg==",
                "type": 0,
                "max_access_count": 3,
                "access_count": 0,
                "revision_date": "2022-11-07T06:52:57.123Z",
                "deletion_date": "2022-11-23T19:06:53.810Z",
                "expiration_date": "2022-11-23T19:06:53.810Z",
                "password_set": True,
                "disabled": False,
                "hide_email": True,
                "text": {"text": "Secret Information", "hidden": True},
            },
        ]

        actual_result = bitwarden_module.list_sends(profile=profile)

        assert actual_result == expected_result


def test_list_sends_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_sends", autospec=True
    ) as list_sends:
        list_sends.return_value = False

        actual_result = bitwarden_module.list_sends(profile=profile)

        assert actual_result == expected_result


def test_get_org_collection_successful_then_return_dict(profile):

    expected_result = {
        "object": "org-collection",
        "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
        "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
        "name": "Default Collection",
        "external_id": None,
        "groups": [],
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_org_collection", autospec=True
    ) as get_org_collection:
        get_org_collection.return_value = {
            "object": "org-collection",
            "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Default Collection",
            "external_id": None,
            "groups": [],
        }

        actual_result = bitwarden_module.get_org_collection(
            profile=profile,
            collection_id="55dff6f9-9d46-4337-a6a7-77144d7c495b",
            organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e",
        )

        assert actual_result == expected_result


def test_get_org_collection_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_org_collection", autospec=True
    ) as get_org_collection:
        get_org_collection.return_value = False

        actual_result = bitwarden_module.get_org_collection(
            profile=profile,
            collection_id="55dff6f9-9d46-4337-a6a7-77144d7c495b",
            organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e",
        )

        assert actual_result == expected_result


def test_list_org_collections_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "collection",
            "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Default Collection",
            "external_id": None,
        },
        {
            "object": "collection",
            "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Test Collection",
            "external_id": None,
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_org_collections",
        autospec=True,
    ) as list_org_collections:
        list_org_collections.return_value = [
            {
                "object": "collection",
                "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
                "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
                "name": "Default Collection",
                "external_id": None,
            },
            {
                "object": "collection",
                "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
                "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
                "name": "Test Collection",
                "external_id": None,
            },
        ]

        actual_result = bitwarden_module.list_org_collections(
            profile=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_org_collections_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_org_collections",
        autospec=True,
    ) as list_org_collections:
        list_org_collections.return_value = False

        actual_result = bitwarden_module.list_org_collections(
            profile=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_collections_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "collection",
            "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Default Collection",
            "external_id": None,
        },
        {
            "object": "collection",
            "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Test Collection",
            "external_id": None,
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_collections", autospec=True
    ) as list_collections:
        list_collections.return_value = [
            {
                "object": "collection",
                "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
                "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
                "name": "Default Collection",
                "external_id": None,
            },
            {
                "object": "collection",
                "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
                "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
                "name": "Test Collection",
                "external_id": None,
            },
        ]

        actual_result = bitwarden_module.list_collections(profile=profile)

        assert actual_result == expected_result


def test_list_collections_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_collections", autospec=True
    ) as list_collections:
        list_collections.return_value = False

        actual_result = bitwarden_module.list_collections(profile=profile)

        assert actual_result == expected_result


def test_list_organizations_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "organization",
            "id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Example Organization",
            "status": 2,
            "type": 0,
            "enabled": True,
        },
        {
            "object": "organization",
            "id": "7c6d6133-f526-4d72-9f87-db6cd649da22",
            "name": "Another Organization",
            "status": 2,
            "type": 0,
            "enabled": True,
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_organizations", autospec=True
    ) as list_organizations:
        list_organizations.return_value = [
            {
                "object": "organization",
                "id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
                "name": "Example Organization",
                "status": 2,
                "type": 0,
                "enabled": True,
            },
            {
                "object": "organization",
                "id": "7c6d6133-f526-4d72-9f87-db6cd649da22",
                "name": "Another Organization",
                "status": 2,
                "type": 0,
                "enabled": True,
            },
        ]

        actual_result = bitwarden_module.list_organizations(profile=profile)

        assert actual_result == expected_result


def test_list_organizations_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_organizations", autospec=True
    ) as list_organizations:
        list_organizations.return_value = False

        actual_result = bitwarden_module.list_organizations(profile=profile)

        assert actual_result == expected_result


def test_list_org_members_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "org-member",
            "email": "user@example.com",
            "name": "Test User",
            "id": "1a3d40c2-5b30-4890-ba3e-05a53f0afda8",
            "status": 2,
            "type": 0,
            "two_factor_enabled": False,
        },
        {
            "object": "org-member",
            "email": "anotheruser@example.com",
            "name": "Another User",
            "id": "33c0d99e-7aa3-403a-8a22-c86c6774f14b",
            "status": 2,
            "type": 2,
            "two_factor_enabled": False,
        },
    ]

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_org_members", autospec=True
    ) as list_org_members:
        list_org_members.return_value = [
            {
                "object": "org-member",
                "email": "user@example.com",
                "name": "Test User",
                "id": "1a3d40c2-5b30-4890-ba3e-05a53f0afda8",
                "status": 2,
                "type": 0,
                "two_factor_enabled": False,
            },
            {
                "object": "org-member",
                "email": "anotheruser@example.com",
                "name": "Another User",
                "id": "33c0d99e-7aa3-403a-8a22-c86c6774f14b",
                "status": 2,
                "type": 2,
                "two_factor_enabled": False,
            },
        ]

        actual_result = bitwarden_module.list_org_members(
            profile=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_org_members_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.list_org_members", autospec=True
    ) as list_org_members:
        list_org_members.return_value = False

        actual_result = bitwarden_module.list_org_members(
            profile=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_sync_successful_then_return_true(profile):

    expected_result = True

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.sync", autospec=True
    ) as sync:
        sync.return_value = True

        actual_result = bitwarden_module.sync(profile=profile)

        assert actual_result == expected_result


def test_sync_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.sync", autospec=True
    ) as sync:
        sync.return_value = False

        actual_result = bitwarden_module.sync(profile=profile)

        assert actual_result == expected_result


def test_get_status_successful_then_return_dict(profile):

    expected_result = {
        "server_url": "https://vault.example.com",
        "last_sync": "2023-01-14T07:05:31.933Z",
        "user_email": "user@example.com",
        "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
        "status": "unlocked",
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {
            "server_url": "https://vault.example.com",
            "last_sync": "2023-01-14T07:05:31.933Z",
            "user_email": "user@example.com",
            "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
            "status": "unlocked",
        }

        actual_result = bitwarden_module.get_status(profile=profile)

        assert actual_result == expected_result


def test_get_status_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_status", autospec=True
    ) as get_status:
        get_status.return_value = False

        actual_result = bitwarden_module.get_status(profile=profile)

        assert actual_result == expected_result


def test_get_status_use_cli_successful_then_return_dict(profile):

    expected_result = {
        "server_url": "https://vault.example.com",
        "last_sync": "2023-01-14T07:05:31.933Z",
        "user_email": "user@example.com",
        "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
        "status": "unlocked",
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {
            "server_url": "https://vault.example.com",
            "last_sync": "2023-01-14T07:05:31.933Z",
            "user_email": "user@example.com",
            "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
            "status": "unlocked",
        }

        actual_result = bitwarden_module.get_status(use_cli=True, profile=profile)

        assert actual_result == expected_result


def test_get_status_use_cli_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_cli.get_status", autospec=True
    ) as get_status:
        get_status.return_value = False

        actual_result = bitwarden_module.get_status(use_cli=True, profile=profile)

        assert actual_result == expected_result


def test_generate_password_successful_then_return_string(profile):

    expected_result = "DJQ#trgzJ&qUoJEj"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.generate_password", autospec=True
    ) as generate_password:
        generate_password.return_value = "DJQ#trgzJ&qUoJEj"

        actual_result = bitwarden_module.generate_password(
            uppercase=True,
            lowercase=True,
            numeric=True,
            special=True,
            length=16,
            profile=profile,
        )

        assert actual_result == expected_result


def test_generate_password_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.generate_password", autospec=True
    ) as generate_password:
        generate_password.return_value = False

        actual_result = bitwarden_module.generate_password(
            uppercase=True,
            lowercase=True,
            numeric=True,
            special=True,
            length=16,
            profile=profile,
        )

        assert actual_result == expected_result


def test_generate_passphrase_successful_then_return_string(profile):

    expected_result = "Correct-Horse-Battery9-Staple"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.generate_passphrase", autospec=True
    ) as generate_passphrase:
        generate_passphrase.return_value = "Correct-Horse-Battery9-Staple"

        actual_result = bitwarden_module.generate_passphrase(
            words=4,
            separator="-",
            capitalize=True,
            number=True,
            profile=profile,
        )

        assert actual_result == expected_result


def test_generate_passphrase_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.generate_passphrase", autospec=True
    ) as generate_passphrase:
        generate_passphrase.return_value = False

        actual_result = bitwarden_module.generate_passphrase(
            words=4,
            separator="-",
            capitalize=True,
            number=True,
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_template_successful_then_return_dict(profile):

    expected_result = {
        "organization_id": None,
        "collection_ids": None,
        "folder_id": None,
        "type": 1,
        "name": "Item name",
        "notes": "Some notes about this item.",
        "favorite": False,
        "fields": [],
        "login": None,
        "secure_note": None,
        "card": None,
        "identity": None,
        "reprompt": 0,
    }

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_template", autospec=True
    ) as get_template:
        get_template.return_value = {
            "organization_id": None,
            "collection_ids": None,
            "folder_id": None,
            "type": 1,
            "name": "Item name",
            "notes": "Some notes about this item.",
            "favorite": False,
            "fields": [],
            "login": None,
            "secure_note": None,
            "card": None,
            "identity": None,
            "reprompt": 0,
        }

        actual_result = bitwarden_module.get_template(template_type="item", profile=profile)

        assert actual_result == expected_result


def test_get_template_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_template", autospec=True
    ) as get_template:
        get_template.return_value = False

        actual_result = bitwarden_module.get_template(template_type="item", profile=profile)

        assert actual_result == expected_result


def test_get_fingerprint_successful_then_return_string(profile):

    expected_result = "geometry-subpanel-huff-unengaged-fragrant"

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_fingerprint", autospec=True
    ) as get_fingerprint:
        get_fingerprint.return_value = "geometry-subpanel-huff-unengaged-fragrant"

        actual_result = bitwarden_module.get_fingerprint(profile=profile)

        assert actual_result == expected_result


def test_get_fingerprint_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch(
        "saltext.bitwarden.modules.bitwarden_mod.bitwarden_vault.get_fingerprint", autospec=True
    ) as get_fingerprint:
        get_fingerprint.return_value = False

        actual_result = bitwarden_module.get_fingerprint(profile=profile)

        assert actual_result == expected_result
