from unittest.mock import patch

import pytest
import saltext.bitwarden.sdb.bitwarden_mod as bitwarden_sdb


@pytest.fixture
def configure_loader_modules():
    module_globals = {
        "__salt__": {"this_does_not_exist.please_replace_it": lambda: True},
    }
    return {
        bitwarden_sdb: module_globals,
    }


@pytest.fixture
def profile():
    return {
        "driver": "bitwarden",
        "vault_api_url": "http://localhost:8087",
    }


@pytest.fixture
def get_item_results():
    return {
        "object": "item",
        "id": "e276050e-1c71-42ac-95eb-ad3300f63840",
        "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
        "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
        "type": 1,
        "reprompt": 0,
        "name": "Test Object",
        "notes": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
        "favorite": False,
        "fields": [
            {"name": "test_text", "value": "test", "type": 0, "linked_id": None},
            {"name": "test_hidden", "value": "test", "type": 1, "linked_id": None},
            {"name": "test_boolean", "value": "true", "type": 2, "linked_id": None},
            {"name": "test_linked_username", "value": None, "type": 3, "linked_id": 100},
            {"name": "test_linked_password", "value": None, "type": 3, "linked_id": 101},
        ],
        "login": {
            "uris": [
                {"match": 1, "uri": "https://test.example.com"},
                {"match": 0, "uri": "https://test2.example.com"},
                {"match": 2, "uri": "https://test3.example.com"},
                {"match": 4, "uri": "https://test4.example.com"},
                {"match": 3, "uri": "https://test5.example.com"},
                {"match": 5, "uri": "https://test6.example.com"},
                {"match": None, "uri": "https://test7.example.com"},
            ],
            "username": "test",
            "password": "CorrectHorseBatteryStaple",
            "totp": "JBSWY3DPEHPK3PXP",
            "password_revision_date": "2023-01-30T05:32:50.470Z",
        },
        "collection_ids": [],
        "attachments": [
            {
                "id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                "file_name": "test.txt",
                "size": "81",
                "size_name": "81 Bytes",
                "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
            }
        ],
        "creation_date": "2023-01-14T05:05:11.666Z",
        "revision_date": "2023-01-30T05:32:51.443Z",
        "deleted_date": None,
        "password_history": [
            {"last_used_date": "2023-01-30T05:32:50.470Z", "password": "testtesttest"},
            {"last_used_date": "2023-01-14T06:46:37.112Z", "password": "testtest"},
        ],
    }


def test_get_name_then_return_string(profile, get_item_results):

    expected_result = "Test Object"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/name",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_username_then_return_string(profile, get_item_results):

    expected_result = "test"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/username",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_password_then_return_string(profile, get_item_results):

    expected_result = "CorrectHorseBatteryStaple"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/password",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_totp_then_return_string(profile, get_item_results):

    expected_result = "123456"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item, patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item_field", autospec=True
    ) as get_item_field:
        get_item.return_value = get_item_results
        get_item_field.return_value = "123456"

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/totp",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_notes_then_return_string(profile, get_item_results):

    expected_result = (
        "This is a test, multiline note field.\nHere is the second line.\nHere is the third line."
    )

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/notes",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_creation_date_then_return_string(profile, get_item_results):

    expected_result = "2023-01-14T05:05:11.666Z"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/creation_date",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_revision_date_then_return_string(profile, get_item_results):

    expected_result = "2023-01-30T05:32:51.443Z"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/revision_date",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_deleted_date_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/deleted_date",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_password_revision_date_then_return_string(profile, get_item_results):

    expected_result = "2023-01-30T05:32:50.470Z"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/password_revision_date",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_password_history_index_0_then_return_string(profile, get_item_results):

    expected_result = "CorrectHorseBatteryStaple"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/password_history/by-index/0",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_password_history_index_1_then_return_string(profile, get_item_results):

    expected_result = "testtesttest"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/password_history/by-index/1",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_password_history_index_3_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/password_history/by-index/3",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_text_field_value_then_return_string(profile, get_item_results):

    expected_result = "test"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_text/value",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_text_field_type_then_return_int(profile, get_item_results):

    expected_result = 0

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_text/type",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_text_field_linked_id_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_text/linked_id",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_hidden_field_value_then_return_string(profile, get_item_results):

    expected_result = "test"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_hidden/value",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_hidden_field_type_then_return_int(profile, get_item_results):

    expected_result = 1

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_hidden/type",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_hidden_field_linked_id_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_hidden/linked_id",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_boolean_field_value_then_return_string(profile, get_item_results):

    expected_result = "true"

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_boolean/value",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_boolean_field_type_then_return_string(profile, get_item_results):

    expected_result = 2

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_boolean/type",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_boolean_field_linked_id_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_boolean/linked_id",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_username_field_value_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_username/value",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_username_field_type_then_return_int(profile, get_item_results):

    expected_result = 3

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_username/type",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_username_field_linked_id_then_return_int(profile, get_item_results):

    expected_result = 100

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_username/linked_id",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_password_field_value_then_return_none(profile, get_item_results):

    expected_result = None

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_password/value",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_password_field_type_then_return_int(profile, get_item_results):

    expected_result = 3

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_password/type",
            profile=profile,
        )

        assert actual_result == expected_result


def test_get_linked_password_field_linked_id_then_return_int(profile, get_item_results):

    expected_result = 101

    with patch(
        "saltext.bitwarden.sdb.bitwarden_mod.bitwarden_vault.get_item", autospec=True
    ) as get_item:
        get_item.return_value = get_item_results

        actual_result = bitwarden_sdb.get(
            key="by-uuid/e276050e-1c71-42ac-95eb-ad3300f63840/fields/by-name/test_linked_password/linked_id",
            profile=profile,
        )

        assert actual_result == expected_result
