from unittest.mock import MagicMock
from unittest.mock import patch

import pytest
import saltext.bitwarden.states.bitwarden_mod as bitwarden_state


@pytest.fixture
def configure_loader_modules():
    module_globals = {
        "__salt__": {
            "config.get": MagicMock(
                return_value={
                    "driver": "bitwarden",
                    "cli_path": "/bin/bw",
                    "cli_conf_dir": "/etc/salt/.bitwarden",
                    "cli_runas": "salt",
                    "vault_url": "https://bitwarden.com",
                    "email": "user@example.com",
                    "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
                    "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
                },
            ),
        }
    }
    return {
        bitwarden_state: module_globals,
    }


@pytest.fixture
def profile():
    return {
        "driver": "bitwarden",
        "cli_path": "/bin/bw",
        "cli_conf_dir": "/etc/salt/.bitwarden",
        "cli_runas": "salt",
        "vault_url": "https://bitwarden.com",
        "email": "user@example.com",
        "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
        "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
    }


def test_logged_in_unlocked_then_no_changes(profile):
    name = "logged_in"
    use_cli = False

    expected_result = {
        "name": name,
        "changes": {},
        "result": True,
        "comment": "bitwarden already logged in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_vault.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {"status": "unlocked"}

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_locked_then_no_changes(profile):
    name = "logged_in"
    use_cli = False

    expected_result = {
        "name": name,
        "changes": {},
        "result": True,
        "comment": "bitwarden already logged in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_vault.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {"status": "locked"}

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_unauthenticated_then_log_in(profile):
    name = "logged_in"
    use_cli = False

    expected_result = {
        "name": name,
        "changes": {
            "old": "unauthenticated",
            "new": "locked",
        },
        "result": True,
        "comment": "bitwarden logged in succesfully.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_vault.get_status",
        autospec=True,
        side_effect=[{"status": "unauthenticated"}, {"status": "locked"}],
    ), patch("saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.login", autospec=True) as login:

        login.return_value = True

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_unauthenticated_login_fails(profile):
    name = "logged_in"
    use_cli = False

    expected_result = {
        "name": name,
        "changes": {},
        "result": False,
        "comment": "bitwarden unable to log in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_vault.get_status", autospec=True
    ) as get_status, patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.login", autospec=True
    ) as login:

        get_status.return_value = {"status": "unauthenticated"}
        login.return_value = False

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_cli_unlocked_then_no_changes(profile):
    name = "logged_in"
    use_cli = True

    expected_result = {
        "name": name,
        "changes": {},
        "result": True,
        "comment": "bitwarden already logged in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {"status": "unlocked"}

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_cli_locked_then_no_changes(profile):
    name = "logged_in"
    use_cli = True

    expected_result = {
        "name": name,
        "changes": {},
        "result": True,
        "comment": "bitwarden already logged in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.get_status", autospec=True
    ) as get_status:
        get_status.return_value = {"status": "locked"}

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_cli_unauthenticated_then_log_in(profile):
    name = "logged_in"
    use_cli = True

    expected_result = {
        "name": name,
        "changes": {
            "old": "unauthenticated",
            "new": "locked",
        },
        "result": True,
        "comment": "bitwarden logged in succesfully.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.get_status",
        autospec=True,
        side_effect=[{"status": "unauthenticated"}, {"status": "locked"}],
    ), patch("saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.login", autospec=True) as login:

        login.return_value = True

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result


def test_logged_in_cli_unauthenticated_login_fails(profile):
    name = "logged_in"
    use_cli = True

    expected_result = {
        "name": name,
        "changes": {},
        "result": False,
        "comment": "bitwarden unable to log in.",
    }

    with patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.get_status", autospec=True
    ) as get_status_cli, patch(
        "saltext.bitwarden.states.bitwarden_mod.bitwarden_cli.login", autospec=True
    ) as login:

        get_status_cli.return_value = {"status": "unauthenticated"}
        login.return_value = False

        actual_result = bitwarden_state.logged_in(name=name, use_cli=use_cli, profile=profile)

        assert actual_result == expected_result
