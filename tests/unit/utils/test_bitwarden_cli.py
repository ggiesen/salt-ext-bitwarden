import pytest
import saltext.bitwarden.utils.bitwarden_cli as bitwarden_cli


@pytest.fixture
def profile():
    return {
        "driver": "bitwarden",
        "cli_path": "/bin/bw",
        "cli_conf_dir": "/etc/salt/.bitwarden",
        "cli_runas": "salt",
        "vault_url": "https://bitwarden.com",
        "email": "user@example.com",
        "password": "CorrectHorseBatteryStaple",
        "vault_api_url": "http://localhost:8087",
        "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
        "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
    }


def test_login_successful_then_return_true(fp, profile):  # pylint: disable=invalid-name

    expected_result = True

    fp.register(
        "bw login user@example.com --apikey --response --nointeraction",
        stdout=[
            b'{"success":true,"data":{"noColor":false,"object":"message","title":"You are logged in!","message":"To unlock your vault, use the `unlock` command. ex:$ bw unlock"}}'  # pylint: disable=line-too-long
        ],
    )

    actual_result = bitwarden_cli.login(opts=profile)

    assert actual_result == expected_result


def test_login_unsuccessful_then_return_false(fp, profile):  # pylint: disable=invalid-name

    expected_result = {
        "Error": {
            "captcha_required": False,
            "response": {"error": "invalid_client"},
            "status_code": 400,
        }
    }

    fp.register(
        "bw login user@example.com --apikey --response --nointeraction",
        stdout=[
            b'{"success":false,"message":{"response":{"error":"invalid_client"},"captchaRequired":false,"statusCode":400}}'
        ],
    )

    actual_result = bitwarden_cli.login(opts=profile)

    assert actual_result == expected_result


def test_login_already_logged_in_then_return_true(fp, profile):  # pylint: disable=invalid-name

    expected_result = True

    fp.register(
        "bw login user@example.com --apikey --response --nointeraction",
        stdout=[b'{"success":false,"message":"You are already logged in as user@example.com."}'],
    )

    actual_result = bitwarden_cli.login(opts=profile)

    assert actual_result == expected_result


def test_logout_successful_then_return_true(fp, profile):  # pylint: disable=invalid-name

    expected_result = True

    fp.register(
        "bw logout --response --nointeraction",
        stdout=[
            b'{"success":true,"data":{"noColor":false,"object":"message","title":"You have logged out.","message":null}}'
        ],
    )

    actual_result = bitwarden_cli.logout(opts=profile)

    assert actual_result == expected_result


def test_logout_already_logged_out_then_return_true(fp, profile):  # pylint: disable=invalid-name

    expected_result = True

    fp.register(
        "bw logout --response --nointeraction",
        stdout=[b'{"success":false,"message":"You are not logged in."}'],
    )

    actual_result = bitwarden_cli.logout(opts=profile)

    assert actual_result == expected_result


def test_logout_unsuccessful_then_return_false(fp, profile):  # pylint: disable=invalid-name

    expected_result = False

    fp.register(
        "bw logout --response --nointeraction",
        stdout=[
            b'{"success":false,"message":{"response":{"error":"some_fake_error"},"captchaRequired":false,"statusCode":400}}'
        ],
    )

    actual_result = bitwarden_cli.logout(opts=profile)

    assert actual_result == expected_result


def test_get_status_successful_then_return_dict(fp, profile):  # pylint: disable=invalid-name

    expected_result = {
        "server_url": "https://vault.example.com",
        "last_sync": "2023-01-14T07:05:31.933Z",
        "user_email": "user@example.com",
        "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
        "status": "unlocked",
    }

    fp.register(
        "bw status --response --nointeraction",
        stdout=[
            b'{"success":true,"data":{"object":"template","template":{"serverUrl":"https://vault.example.com","lastSync":"2023-01-14T07:05:31.933Z","userEmail":"user@example.com","userId":"3bb033cf-2746-4df6-b599-3cab7eb28561","status":"unlocked"}}}'  # pylint: disable=line-too-long
        ],
    )

    actual_result = bitwarden_cli.get_status(opts=profile)

    assert actual_result == expected_result


def test_get_status_unsuccessful_then_return_false(fp, profile):  # pylint: disable=invalid-name

    expected_result = False

    fp.register(
        "bw status --response --nointeraction",
        stdout=[
            b'{"success":false,"message":{"response":{"error":"invalid_client"},"captchaRequired":false,"statusCode":400}}'
        ],
    )

    actual_result = bitwarden_cli.get_status(opts=profile)

    assert actual_result == expected_result
