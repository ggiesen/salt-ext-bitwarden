# pylint: disable=too-many-lines
from unittest.mock import patch

import pytest
import requests
import saltext.bitwarden.utils.bitwarden_vault as bitwarden_vault


@pytest.fixture
def profile():
    return {
        "driver": "bitwarden",
        "cli_path": "/bin/bw",
        "cli_conf_dir": "/etc/salt/.bitwarden",
        "cli_runas": "salt",
        "vault_url": "https://bitwarden.com",
        "email": "user@example.com",
        "password": "CorrectHorseBatteryStaple",
        "vault_api_url": "http://localhost:8087",
        "client_id": "user.25fa6fc6-deeb-4b42-a279-5e680b51aa58",
        "client_secret": "AofieD0oexiex1mie3eigi9oojooF3",
    }


@pytest.fixture
def get_item_results():
    return {
        "object": "item",
        "id": "e276050e-1c71-42ac-95eb-ad3300f63840",
        "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
        "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
        "type": 1,
        "reprompt": 0,
        "name": "Test Object",
        "notes": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
        "favorite": False,
        "fields": [
            {"name": "test_text", "value": "test", "type": 0, "linked_id": None},
            {"name": "test_hidden", "value": "test", "type": 1, "linked_id": None},
            {"name": "test_boolean", "value": "true", "type": 2, "linked_id": None},
            {"name": "test_linked_username", "value": None, "type": 3, "linked_id": 100},
            {"name": "test_linked_password", "value": None, "type": 3, "linked_id": 101},
        ],
        "login": {
            "uris": [
                {"match": 1, "uri": "https://test.example.com"},
                {"match": 0, "uri": "https://test2.example.com"},
                {"match": 2, "uri": "https://test3.example.com"},
                {"match": 4, "uri": "https://test4.example.com"},
                {"match": 3, "uri": "https://test5.example.com"},
                {"match": 5, "uri": "https://test6.example.com"},
                {"match": None, "uri": "https://test7.example.com"},
            ],
            "username": "test",
            "password": "CorrectHorseBatteryStaple",
            "totp": "JBSWY3DPEHPK3PXP",
            "password_revision_date": "2023-01-30T05:32:50.470Z",
        },
        "collection_ids": [],
        "attachments": [
            {
                "id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                "file_name": "test.txt",
                "size": "81",
                "size_name": "81 Bytes",
                "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
            }
        ],
        "creation_date": "2023-01-14T05:05:11.666Z",
        "revision_date": "2023-01-30T05:32:51.443Z",
        "deleted_date": None,
        "password_history": [
            {"last_used_date": "2023-01-30T05:32:50.470Z", "password": "testtesttest"},
            {"last_used_date": "2023-01-14T06:46:37.112Z", "password": "testtest"},
        ],
    }


@pytest.fixture
def list_items_results():
    return [
        {
            "object": "item",
            "id": "b8bb12f4-b378-4e56-b86d-af48005e13c2",
            "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
            "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
            "type": 1,
            "reprompt": 0,
            "name": "Example Login Item",
            "notes": "Example notes for login item",
            "favorite": False,
            "login": {
                "uris": [],
                "username": "jdoe",
                "password": "CorrectHorseBatteryStaple",
                "totp": None,
                "password_revision_date": None,
            },
            "collection_ids": [],
            "revision_date": "2022-11-09T05:42:31.470Z",
            "deleted_date": None,
        },
        {
            "object": "item",
            "id": "e276050e-1c71-42ac-95eb-ad3300f63840",
            "organization_id": "fd0331e4-edf7-4924-a1a9-384feb0d3baf",
            "folder_id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
            "type": 1,
            "reprompt": 0,
            "name": "Test Object",
            "notes": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
            "favorite": False,
            "fields": [
                {"name": "test_text", "value": "test", "type": 0, "linked_id": None},
                {"name": "test_hidden", "value": "test", "type": 1, "linked_id": None},
                {"name": "test_boolean", "value": "true", "type": 2, "linked_id": None},
                {"name": "test_linked_username", "value": None, "type": 3, "linked_id": 100},
                {"name": "test_linked_password", "value": None, "type": 3, "linked_id": 101},
            ],
            "login": {
                "uris": [
                    {"match": 1, "uri": "https://test.example.com"},
                    {"match": 0, "uri": "https://test2.example.com"},
                    {"match": 2, "uri": "https://test3.example.com"},
                    {"match": 4, "uri": "https://test4.example.com"},
                    {"match": 3, "uri": "https://test5.example.com"},
                    {"match": 5, "uri": "https://test6.example.com"},
                    {"match": None, "uri": "https://test7.example.com"},
                ],
                "username": "test",
                "password": "testtesttest",
                "totp": "JBSWY3DPEHPK3PXP",
                "password_revision_date": "2023-01-14T06:46:37.112Z",
            },
            "collection_ids": [],
            "attachments": [
                {
                    "id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                    "file_name": "test.txt",
                    "size": "81",
                    "size_name": "81 Bytes",
                    "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g",
                }
            ],
            "revision_date": "2023-01-14T07:05:22.130Z",
            "deleted_date": None,
            "password_history": [
                {"last_used_date": "2023-01-14T06:46:37.112Z", "password": "testtest"}
            ],
        },
    ]


def test_lock_successful_then_return_true(profile):

    expected_result = True

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"noColor":false,"object":"message","title":"Your vault is locked.","message":null}}'
        }

        actual_result = bitwarden_vault.lock(opts=profile)

        assert actual_result == expected_result


def test_lock_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.lock(opts=profile)

        assert actual_result == expected_result


def test_unlock_successful_then_return_true(profile):

    expected_result = True

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"noColor":false,"object":"message","title":"Your vault is now unlocked!","message":"To unlock your vault, set your session key to the `BW_SESSION` environment variable. ex:$ export BW_SESSION=\\"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==\\"> $env:BW_SESSION=\\"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==\\"You can also pass the session key to any command with the `--session` option. ex:$ bw list items --session 4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==","raw":"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow=="}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.unlock(opts=profile)

        assert actual_result == expected_result


def test_unlock_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.unlock(opts=profile)

        assert actual_result == expected_result


def test_get_item_successful_then_return_dict(profile, get_item_results):

    expected_result = get_item_results

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success": true, "data": {"object": "item", "id": "e276050e-1c71-42ac-95eb-ad3300f63840", "organizationId": "fd0331e4-edf7-4924-a1a9-384feb0d3baf", "folderId": "0d62e546-ec44-4709-b6c6-ad3300f10e11", "type": 1, "reprompt": 0, "name": "Test Object", "notes": "This is a test, multiline note field.\\nHere is the second line.\\nHere is the third line.", "favorite": false, "fields": [{"name": "test_text", "value": "test", "type": 0, "linkedId": null}, {"name": "test_hidden", "value": "test", "type": 1, "linkedId": null}, {"name": "test_boolean", "value": "true", "type": 2, "linkedId": null}, {"name": "test_linked_username", "value": null, "type": 3, "linkedId": 100}, {"name": "test_linked_password", "value": null, "type": 3, "linkedId": 101}], "login": {"uris": [{"match": 1, "uri": "https://test.example.com"}, {"match": 0, "uri": "https://test2.example.com"}, {"match": 2, "uri": "https://test3.example.com"}, {"match": 4, "uri": "https://test4.example.com"}, {"match": 3, "uri": "https://test5.example.com"}, {"match": 5, "uri": "https://test6.example.com"}, {"match": null, "uri": "https://test7.example.com"}], "username": "test", "password": "CorrectHorseBatteryStaple", "totp": "JBSWY3DPEHPK3PXP", "passwordRevisionDate": "2023-01-30T05:32:50.470Z"}, "collectionIds": [], "attachments": [{"id": "pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g", "fileName": "test.txt", "size": "81", "sizeName": "81 Bytes", "url": "https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g"}], "creationDate": "2023-01-14T05:05:11.666Z", "revisionDate": "2023-01-30T05:32:51.443Z", "deletedDate": null, "passwordHistory": [{"lastUsedDate": "2023-01-30T05:32:50.470Z", "password": "testtesttest"},{"lastUsedDate": "2023-01-14T06:46:37.112Z", "password": "testtest"}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.get_item(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_get_item_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_item(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840"
        )

        assert actual_result == expected_result


def test_list_items_successful_then_return_dict(profile, list_items_results):

    expected_result = list_items_results

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"item","id":"b8bb12f4-b378-4e56-b86d-af48005e13c2","organizationId":"fd0331e4-edf7-4924-a1a9-384feb0d3baf","folderId":"0d62e546-ec44-4709-b6c6-ad3300f10e11","type":1,"reprompt":0,"name":"Example Login Item","notes":"Example notes for login item","favorite":false,"login":{"uris":[],"username":"jdoe","password":"CorrectHorseBatteryStaple","totp":null,"passwordRevisionDate":null},"collectionIds":[],"revisionDate":"2022-11-09T05:42:31.470Z","deletedDate":null},{"object":"item","id":"e276050e-1c71-42ac-95eb-ad3300f63840","organizationId":"fd0331e4-edf7-4924-a1a9-384feb0d3baf","folderId":"0d62e546-ec44-4709-b6c6-ad3300f10e11","type":1,"reprompt":0,"name":"Test Object","notes":"This is a test, multiline note field.\\nHere is the second line.\\nHere is the third line.","favorite":false,"fields":[{"name":"test_text","value":"test","type":0,"linkedId":null},{"name":"test_hidden","value":"test","type":1,"linkedId":null},{"name":"test_boolean","value":"true","type":2,"linkedId":null},{"name":"test_linked_username","value":null,"type":3,"linkedId":100},{"name":"test_linked_password","value":null,"type":3,"linkedId":101}],"login":{"uris":[{"match":1,"uri":"https://test.example.com"},{"match":0,"uri":"https://test2.example.com"},{"match":2,"uri":"https://test3.example.com"},{"match":4,"uri":"https://test4.example.com"},{"match":3,"uri":"https://test5.example.com"},{"match":5,"uri":"https://test6.example.com"},{"match":null,"uri":"https://test7.example.com"}],"username":"test","password":"testtesttest","totp":"JBSWY3DPEHPK3PXP","passwordRevisionDate":"2023-01-14T06:46:37.112Z"},"collectionIds":[],"attachments":[{"id":"pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g","fileName":"test.txt","size":"81","sizeName":"81 Bytes","url":"https://vault.example.com/attachments/e276050e-1c71-42ac-95eb-ad3300f63840/pqx5xe7rfolnxztm3a7zs5zjbwyz0x5g"}],"revisionDate":"2023-01-14T07:05:22.130Z","deletedDate":null,"passwordHistory":[{"lastUsedDate":"2023-01-14T06:46:37.112Z","password":"testtest"}]}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_items(opts=profile)

        assert actual_result == expected_result


def test_list_items_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_items(opts=profile)

        assert actual_result == expected_result


def test_get_item_field_username_successful_then_return_string(profile):

    expected_result = "test"

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {"object": "string", "data": "test"},
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="username"
        )

        assert actual_result == expected_result


def test_get_item_field_password_successful_then_return_string(profile):

    expected_result = "CorrectHorseBatteryStaple"

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {"object": "string", "data": "CorrectHorseBatteryStaple"},
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="password"
        )

        assert actual_result == expected_result


def test_get_item_field_uri_successful_then_return_string(profile):

    expected_result = "https://test.example.com"

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {"object": "string", "data": "https://test.example.com"},
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="uri"
        )

        assert actual_result == expected_result


def test_get_item_field_totp_successful_then_return_string(profile):

    expected_result = "123456"

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {"object": "string", "data": "123456"},
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="totp"
        )

        assert actual_result == expected_result


def test_get_item_field_notes_successful_then_return_string(profile):

    expected_result = (
        "This is a test, multiline note field.\nHere is the second line.\nHere is the third line."
    )

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {
                "object": "string",
                "data": "This is a test, multiline note field.\nHere is the second line.\nHere is the third line.",
            },
        }  # pylint: disable=line-too-long

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="notes"
        )

        assert actual_result == expected_result


def test_get_item_field_exposed_successful_then_return_string(profile):

    expected_result = "1868"

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 200
        requests_get.json.return_value = {
            "success": True,
            "data": {"object": "string", "data": "1868"},
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="exposed"
        )

        assert actual_result == expected_result


def test_get_item_field_field_not_set_then_return_false(profile):

    expected_result = None

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 400
        requests_get.json.return_value = {
            "success": False,
            "message": "No username available for this login.",
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="username"
        )

        assert actual_result == expected_result


def test_get_item_field_item_not_found_then_return_false(profile):

    expected_result = False

    with patch("requests.get", autospec=True) as get:
        requests_get = get.return_value
        requests_get.status_code = 400
        requests_get.json.return_value = {
            "success": False,
            "message": "Not found.",
        }

        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="username"
        )

        assert actual_result == expected_result


def test_get_item_field_not_successful_then_return_false(profile):

    expected_result = False

    with patch(
        "requests.get",
        autospec=True,
        side_effect=requests.exceptions.RequestException("Test Exception"),
    ):
        actual_result = bitwarden_vault.get_item_field(
            opts=profile, item_id="e276050e-1c71-42ac-95eb-ad3300f63840", field="username"
        )

        assert actual_result == expected_result


def test_get_folder_successful_then_return_dict(profile):

    expected_result = {
        "object": "folder",
        "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11",
        "name": "Test",
    }

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"folder","id":"0d62e546-ec44-4709-b6c6-ad3300f10e11","name":"Test"}}'
        }

        actual_result = bitwarden_vault.get_folder(
            opts=profile, folder_id="0d62e546-ec44-4709-b6c6-ad3300f10e11"
        )

        assert actual_result == expected_result


def test_get_folder_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_folder(
            opts=profile, folder_id="0d62e546-ec44-4709-b6c6-ad3300f10e11"
        )

        assert actual_result == expected_result


def test_list_folders_successful_then_return_list(profile):

    expected_result = [
        {"object": "folder", "id": "0d62e546-ec44-4709-b6c6-ad3300f10e11", "name": "Test"},
        {
            "object": "folder",
            "id": "b5e12f87-b395-4a26-be2e-aa8d0706eb5a",
            "name": "Another Test Folder",
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"folder","id":"0d62e546-ec44-4709-b6c6-ad3300f10e11","name":"Test"},{"object":"folder","id":"b5e12f87-b395-4a26-be2e-aa8d0706eb5a","name":"Another Test Folder"}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_folders(opts=profile)

        assert actual_result == expected_result


def test_list_folders_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_folders(opts=profile)

        assert actual_result == expected_result


def test_get_send_successful_then_return_dict(profile):

    expected_result = {
        "object": "send",
        "id": "8be1ff82-dd40-4af1-96ae-565467293510",
        "access_id": "cMJbuCb6hUOJN69GGGa9XA",
        "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
        "name": "My Text Send",
        "notes": "Notes for the text send.",
        "key": "rBvU5J6P2GUuaANXyacQ6w==",
        "type": 0,
        "max_access_count": 3,
        "access_count": 0,
        "revision_date": "2022-11-07T05:15:48.503Z",
        "deletion_date": "2022-11-07T06:15:47.930Z",
        "expiration_date": "2022-11-07T06:15:47.933Z",
        "password_set": True,
        "disabled": False,
        "hide_email": True,
        "text": {"text": "Secret Information", "hidden": True},
    }

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"send","id":"8be1ff82-dd40-4af1-96ae-565467293510","accessId":"cMJbuCb6hUOJN69GGGa9XA","accessUrl":"https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w","name":"My Text Send","notes":"Notes for the text send.","key":"rBvU5J6P2GUuaANXyacQ6w==","type":0,"maxAccessCount":3,"accessCount":0,"revisionDate":"2022-11-07T05:15:48.503Z","deletionDate":"2022-11-07T06:15:47.930Z","expirationDate":"2022-11-07T06:15:47.933Z","passwordSet":true,"disabled":false,"hideEmail":true,"text":{"text":"Secret Information","hidden":true}}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.get_send(
            opts=profile, send_id="8be1ff82-dd40-4af1-96ae-565467293510"
        )

        assert actual_result == expected_result


def test_get_send_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_send(
            opts=profile, send_id="8be1ff82-dd40-4af1-96ae-565467293510"
        )

        assert actual_result == expected_result


def test_list_sends_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "send",
            "id": "8be1ff82-dd40-4af1-96ae-565467293510",
            "access_id": "cMJbuCb6hUOJN69GGGa9XA",
            "access_url": "https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w",
            "name": "My Text Send",
            "notes": "Notes for the text send.",
            "key": "rBvU5J6P2GUuaANXyacQ6w==",
            "type": 0,
            "max_access_count": 3,
            "access_count": 0,
            "revision_date": "2022-11-07T05:15:48.503Z",
            "deletion_date": "2022-11-07T06:15:47.930Z",
            "expiration_date": "2022-11-07T06:15:47.933Z",
            "password_set": True,
            "disabled": False,
            "hide_email": True,
            "text": {"text": "Secret Information", "hidden": True},
        },
        {
            "object": "send",
            "id": "eebd1611-dd47-47d9-bee2-0ef3891a95e8",
            "access_id": "RpI1hSjOWAqGga9GAHFGJQ",
            "access_url": "https://vault.example.com/#/send/RpI1hSjOWAqGga9GAHFGJQ/GrOZ69q7XwYdeE65t9-szg",
            "name": "My Text Send",
            "notes": "Notes for the text send.",
            "key": "GrOZ69q7XwYdeE65t9+szg==",
            "type": 0,
            "max_access_count": 3,
            "access_count": 0,
            "revision_date": "2022-11-07T06:52:57.123Z",
            "deletion_date": "2022-11-23T19:06:53.810Z",
            "expiration_date": "2022-11-23T19:06:53.810Z",
            "password_set": True,
            "disabled": False,
            "hide_email": True,
            "text": {"text": "Secret Information", "hidden": True},
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"send","id":"8be1ff82-dd40-4af1-96ae-565467293510","accessId":"cMJbuCb6hUOJN69GGGa9XA","accessUrl":"https://vault.example.com/#/send/cMJbuCb6hUOJN69GGGa9XA/rBvU5J6P2GUuaANXyacQ6w","name":"My Text Send","notes":"Notes for the text send.","key":"rBvU5J6P2GUuaANXyacQ6w==","type":0,"maxAccessCount":3,"accessCount":0,"revisionDate":"2022-11-07T05:15:48.503Z","deletionDate":"2022-11-07T06:15:47.930Z","expirationDate":"2022-11-07T06:15:47.933Z","passwordSet":true,"disabled":false,"hideEmail":true,"text":{"text":"Secret Information","hidden":true}},{"object":"send","id":"eebd1611-dd47-47d9-bee2-0ef3891a95e8","accessId":"RpI1hSjOWAqGga9GAHFGJQ","accessUrl":"https://vault.example.com/#/send/RpI1hSjOWAqGga9GAHFGJQ/GrOZ69q7XwYdeE65t9-szg","name":"My Text Send","notes":"Notes for the text send.","key":"GrOZ69q7XwYdeE65t9+szg==","type":0,"maxAccessCount":3,"accessCount":0,"revisionDate":"2022-11-07T06:52:57.123Z","deletionDate":"2022-11-23T19:06:53.810Z","expirationDate":"2022-11-23T19:06:53.810Z","passwordSet":true,"disabled":false,"hideEmail":true,"text":{"text":"Secret Information","hidden":true}}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_sends(opts=profile)

        assert actual_result == expected_result


def test_list_sends_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_sends(opts=profile)

        assert actual_result == expected_result


def test_get_org_collection_successful_then_return_dict(profile):

    expected_result = {
        "object": "org-collection",
        "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
        "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
        "name": "Default Collection",
        "external_id": None,
        "groups": [],
    }

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"org-collection","id":"55dff6f9-9d46-4337-a6a7-77144d7c495b","organizationId":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Default Collection","externalId":null,"groups":[]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.get_org_collection(
            opts=profile,
            collection_id="55dff6f9-9d46-4337-a6a7-77144d7c495b",
            organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e",
        )

        assert actual_result == expected_result


def test_get_org_collection_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_org_collection(
            opts=profile,
            collection_id="55dff6f9-9d46-4337-a6a7-77144d7c495b",
            organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e",
        )

        assert actual_result == expected_result


def test_list_org_collections_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "collection",
            "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Default Collection",
            "external_id": None,
        },
        {
            "object": "collection",
            "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Test Collection",
            "external_id": None,
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"collection","id":"55dff6f9-9d46-4337-a6a7-77144d7c495b","organizationId":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Default Collection","externalId":null},{"object":"collection","id":"40c8828a-2b20-4957-91ba-d16853ba2b2c","organizationId":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Test Collection","externalId":null}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_org_collections(
            opts=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_org_collections_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_org_collections(
            opts=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_collections_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "collection",
            "id": "55dff6f9-9d46-4337-a6a7-77144d7c495b",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Default Collection",
            "external_id": None,
        },
        {
            "object": "collection",
            "id": "40c8828a-2b20-4957-91ba-d16853ba2b2c",
            "organization_id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Test Collection",
            "external_id": None,
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"collection","id":"55dff6f9-9d46-4337-a6a7-77144d7c495b","organizationId":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Default Collection","externalId":null},{"object":"collection","id":"40c8828a-2b20-4957-91ba-d16853ba2b2c","organizationId":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Test Collection","externalId":null}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_collections(opts=profile)

        assert actual_result == expected_result


def test_list_collections_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_collections(opts=profile)

        assert actual_result == expected_result


def test_list_organizations_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "organization",
            "id": "8347609b-afe2-4f44-94a0-0b3ff10c901e",
            "name": "Example Organization",
            "status": 2,
            "type": 0,
            "enabled": True,
        },
        {
            "object": "organization",
            "id": "7c6d6133-f526-4d72-9f87-db6cd649da22",
            "name": "Another Organization",
            "status": 2,
            "type": 0,
            "enabled": True,
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"organization","id":"8347609b-afe2-4f44-94a0-0b3ff10c901e","name":"Example Organization","status":2,"type":0,"enabled":true},{"object":"organization","id":"7c6d6133-f526-4d72-9f87-db6cd649da22","name":"Another Organization","status":2,"type":0,"enabled":true}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_organizations(opts=profile)

        assert actual_result == expected_result


def test_list_organizations_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_organizations(opts=profile)

        assert actual_result == expected_result


def test_list_org_members_successful_then_return_list(profile):

    expected_result = [
        {
            "object": "org-member",
            "email": "user@example.com",
            "name": "Test User",
            "id": "1a3d40c2-5b30-4890-ba3e-05a53f0afda8",
            "status": 2,
            "type": 0,
            "two_factor_enabled": False,
        },
        {
            "object": "org-member",
            "email": "anotheruser@example.com",
            "name": "Another User",
            "id": "33c0d99e-7aa3-403a-8a22-c86c6774f14b",
            "status": 2,
            "type": 2,
            "two_factor_enabled": False,
        },
    ]

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"list","data":[{"object":"org-member","email":"user@example.com","name":"Test User","id":"1a3d40c2-5b30-4890-ba3e-05a53f0afda8","status":2,"type":0,"twoFactorEnabled":false},{"object":"org-member","email":"anotheruser@example.com","name":"Another User","id":"33c0d99e-7aa3-403a-8a22-c86c6774f14b","status":2,"type":2,"twoFactorEnabled":false}]}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.list_org_members(
            opts=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_list_org_members_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.list_org_members(
            opts=profile, organization_id="8347609b-afe2-4f44-94a0-0b3ff10c901e"
        )

        assert actual_result == expected_result


def test_sync_successful_then_return_true(profile):

    expected_result = True

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"noColor":false,"object":"message","title":"Your vault is now unlocked!","message":"To unlock your vault, set your session key to the `BW_SESSION` environment variable. ex:$ export BW_SESSION=\\"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==\\"> $env:BW_SESSION=\\"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==\\"You can also pass the session key to any command with the `--session` option. ex:$ bw list items --session 4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow==","raw":"4nA+wknCaz6lHeR188YH8XbNTx/LzQVrMcK/QlU7gbID/YZPiQGh9PrG0k+7prGV8J0cplkV804if5rPrEZFow=="}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.sync(opts=profile)

        assert actual_result == expected_result


def test_sync_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.sync(opts=profile)

        assert actual_result == expected_result


def test_get_status_successful_then_return_dict(profile):

    expected_result = {
        "server_url": "https://vault.example.com",
        "last_sync": "2023-01-14T07:05:31.933Z",
        "user_email": "user@example.com",
        "user_id": "3bb033cf-2746-4df6-b599-3cab7eb28561",
        "status": "unlocked",
    }

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"template","template":{"serverUrl":"https://vault.example.com","lastSync":"2023-01-14T07:05:31.933Z","userEmail":"user@example.com","userId":"3bb033cf-2746-4df6-b599-3cab7eb28561","status":"unlocked"}}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.get_status(opts=profile)

        assert actual_result == expected_result


def test_get_status_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_status(opts=profile)

        assert actual_result == expected_result


def test_generate_password_with_defaults_successful_then_return_string(
    profile,
):  # pylint: disable=invalid-name

    expected_result = "dHhjnzCEnG8tdK"

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"string","data":"dHhjnzCEnG8tdK"}}'
        }

        actual_result = bitwarden_vault.generate_password(opts=profile)

        assert actual_result == expected_result


def test_generate_password_with_all_options_successful_then_return_string(
    profile,
):  # pylint: disable=invalid-name

    expected_result = "SswEnxhzXRK$bXGPe$uUjXJvh%%Z$Zqk"

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"string","data":"SswEnxhzXRK$bXGPe$uUjXJvh%%Z$Zqk"}}'
        }

        actual_result = bitwarden_vault.generate_password(
            opts=profile, uppercase=True, lowercase=True, numeric=True, special=True, length=16
        )

        assert actual_result == expected_result


def test_generate_password_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.generate_password(opts=profile)

        assert actual_result == expected_result


def test_generate_passphrase_with_defaults_successful_then_return_string(
    profile,
):  # pylint: disable=invalid-name

    expected_result = "purity-smitten-lilac"

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"string","data":"purity-smitten-lilac"}}'
        }

        actual_result = bitwarden_vault.generate_passphrase(opts=profile)

        assert actual_result == expected_result


def test_generate_passphrase_with_all_options_successful_then_return_string(
    profile,
):  # pylint: disable=invalid-name

    expected_result = "Correct-Horse-Battery9-Staple"

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"string","data":"Correct-Horse-Battery9-Staple"}}'
        }

        actual_result = bitwarden_vault.generate_passphrase(
            opts=profile, words=4, separator="-", capitalize=True, number=True
        )

        assert actual_result == expected_result


def test_generate_passphrase_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.generate_passphrase(opts=profile)

        assert actual_result == expected_result


def test_get_template_successful_then_return_dict(profile):

    expected_result = {
        "organization_id": None,
        "collection_ids": None,
        "folder_id": None,
        "type": 1,
        "name": "Item name",
        "notes": "Some notes about this item.",
        "favorite": False,
        "fields": [],
        "login": None,
        "secure_note": None,
        "card": None,
        "identity": None,
        "reprompt": 0,
    }

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"template","template":{"organizationId":null,"collectionIds":null,"folderId":null,"type":1,"name":"Item name","notes":"Some notes about this item.","favorite":false,"fields":[],"login":null,"secureNote":null,"card":null,"identity":null,"reprompt":0}}}'  # pylint: disable=line-too-long
        }

        actual_result = bitwarden_vault.get_template(opts=profile, template_type="item")

        assert actual_result == expected_result


def test_get_template_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_template(opts=profile, template_type="item")

        assert actual_result == expected_result


def test_get_fingerprint_successful_then_return_string(profile):

    expected_result = "geometry-subpanel-huff-unengaged-fragrant"

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "body": '{"success":true,"data":{"object":"string","data":"geometry-subpanel-huff-unengaged-fragrant"}}'
        }

        actual_result = bitwarden_vault.get_fingerprint(opts=profile)

        assert actual_result == expected_result


def test_get_fingerprint_unsuccessful_then_return_false(profile):

    expected_result = False

    with patch("salt.utils.http.query", autospec=True) as query:
        query.return_value = {
            "error": "HTTP 404: Not Found",
            "status": 404,
        }

        actual_result = bitwarden_vault.get_fingerprint(opts=profile)

        assert actual_result == expected_result
